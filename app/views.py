from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from .models import Blog, Comment
from django.urls import reverse_lazy, reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from django.contrib import messages


class BlogListView(ListView):
    model = Blog
    template_name = "list.html"


def blog_detail_func(request, pk):
    blog = Blog.objects.get(id=pk)
    try:
        comments = Comment.objects.all().filter(blog_id=pk)
    except:
        comments = None
    finally:
        return render(request, "detail.html", {
            "object": blog,
            "comments": comments,
        })


class BlogCreateView(LoginRequiredMixin, CreateView):
    model = Blog
    template_name = "create.html"
    fields = ["title", "text"]
    success_url = reverse_lazy("list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.updated_at = timezone.now()
        messages.success(self.request, "作成しました")
        return super().form_valid(form)


def blog_update_func(request, pk):
    if request.user.is_authenticated:
        u = Blog.objects.get(id=pk)
        if request.user.id != u.author_id:
            return redirect("detail", pk=pk)
        if request.method == "POST":
            u.title = request.POST["title"]
            u.text = request.POST["text"]
            u.updated_at = timezone.now()
            u.save()
            messages.success(request, "編集しました")
            return redirect("detail", pk=pk)
        return render(request, 'update.html', {"before": u})
    else:
        messages.info(request, "編集するにはログインしてください")
        return redirect("detail", pk=pk)


def blog_delete_func(request, pk):
    if request.user.is_authenticated:
        d = Blog.objects.get(id=pk)
        if request.user.id != d.author_id:
            return redirect("list")
        if request.method == "POST":
            d.delete()
            return redirect("list")
        return render(request, "delete.html", {"d": d})
    else:
        messages.info(request, "削除するにはログインしてください")
        return redirect("detail", pk=pk)


def signup(request):
    if request.method == "POST":
        request_username = request.POST['username']
        request_password = request.POST['password']
        try:
            User.objects.get(username=request_username)
            return render(request, 'signup.html', {"message": "すでに登録されています"})
        except:
            User.objects.create_user(request_username, '', request_password)
            return render(request, 'login.html', {'message': "ユーザー登録しました"})
    return render(request, "signup.html")


def login_func(request):
    if request.user.is_authenticated:
        return redirect('list')
    if request.method == "POST":
        request_username = request.POST['username']
        request_password = request.POST['password']
        user = authenticate(request, username=request_username, password=request_password)
        if user is not None:
            login(request, user)
            return redirect('list')
        else:
            return redirect('login')
    return render(request, 'login.html')


def logout_func(request):
    logout(request)
    return redirect('login')


def comment_create_func(request):
    if request.method == "POST":
        comment = request.POST["commnet"]
        blog_id = request.POST["blog_id"]
        if not request.user.is_authenticated:
            messages.info(request, "コメントするにはログインしてください")
            return redirect('detail', pk=blog_id)
        comment_author_id = request.user.id
        Comment.objects.create(
            blog_id=blog_id, comment_author_id=comment_author_id, comment=comment
        )
        return redirect('detail', pk=blog_id)
    return redirect('list')


def comment_delete_func(request, pk):
    if request.method == "POST":
        comment = Comment.objects.get(id=pk)
        blog_id = comment.blog_id
        if not request.user.is_authenticated:
            messages.info(request, "コメントを削除するにはログインしてください")
            redirect('detail', pk=pk)
        if request.user.id == comment.comment_author_id:
            comment.delete()
        return redirect('detail', pk=blog_id)
    return redirect('list')
