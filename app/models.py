from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

class Blog(models.Model):
    title = models.CharField(max_length=100, verbose_name="タイトル")
    text = models.TextField(verbose_name="内容")
    author = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(null=True, blank=True)

    def update(self):
        self.updated_at = timezone.now
        self.save()

    def __str__(self):
        return self.title


class Comment(models.Model):
    blog = models.ForeignKey('Blog', on_delete=models.CASCADE, blank=True, null=True)
    comment_author = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    comment = models.TextField()
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(null=True, blank=True)

    def update(self):
        self.updated_at = timezone.now
        self.save()
