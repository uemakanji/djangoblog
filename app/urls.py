from django.urls import path
from .views import BlogListView, BlogCreateView
from .views import signup, login_func, logout_func, blog_update_func, blog_delete_func, blog_detail_func
from .views import comment_create_func, comment_delete_func

urlpatterns = [
    path("", BlogListView.as_view(), name="list"),
    path("detail/<int:pk>/", blog_detail_func, name="detail"),
    path("create/", BlogCreateView.as_view(), name="create"),
    path("update/<int:pk>/", blog_update_func, name="update"),
    path("delete/<int:pk>/", blog_delete_func, name="delete"),
    path("signup/", signup, name="signup"),
    path("login/", login_func, name="login"),
    path("logout/", logout_func, name="logout"),
    path("comment_create/", comment_create_func, name="comment_create"),
    path("comment_delete/<int:pk>", comment_delete_func, name="comment_delete"),
]
